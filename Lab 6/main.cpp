// main.cpp
// Vector2d driver file
// for testing the Vector2d class
// Daniel Livingstone
#include <iostream>
#include "Vector2d.h"

using namespace std; // using the standard library namespace


// A few test functions
void changeVectorByValue(Vector2d v)
{
	cout << "In function changeVectorByValue - ";
	v.x += 1.0; v.y += 1.0;
	cout << " v = " << v.x << "," << v.y << endl;
}

void changeVectorByReference(Vector2d& v)
{
	cout << "In function changeVectorByReference - ";
	v.x += 1.0; v.y += 1.0;
	cout << " v = " << v.x << "," << v.y << endl;
}

void changeVectorByPointer(Vector2d* v)
{
	cout << "In function changeVectorByPointer - ";
	v->x += 1.0; v->y += 1.0;
	cout << " v = " << v->x << "," << v->y << endl;
}


int main() {

	float s;

	// Test creation of vectors
	// this should work already...
	Vector2d u(2,-3),v(3,2);

	cout << "v: " << v.x << " , " << v.y << endl;

	// code to test call by ref, value, pointer...
	cout << "Calling changeByValue" << endl;
	changeVectorByValue(v);
	cout << "After changeByValue: " << endl;
	cout << "v: " << v.x << " , " << v.y << endl << endl;
	
	cout << "Calling changeByReference" << endl;
	changeVectorByReference(v);
	cout << "After changeByReference: " << endl;
	cout << "v: " << v.x << " , " << v.y << endl << endl;

	cout << "Calling changeByPointer" << endl;
	changeVectorByPointer(&v);
	cout << "After changeByPointer: " << endl;
	cout << "v: " << v.x << " , " << v.y << endl << endl;


	// Test copy constructor
	Vector2d copy(v);
	cout << "copy of v: " << copy.x << " , " << copy.y << endl;


	// Normal test code below:
	cout << "u: " << u.x << " , " << u.y << endl;

	// Test scaling up of vector ( *= operator )
    // If you use the supplied vector2d.cpp, this should work also
	cout << "Test scaling ( u *= 2 ) ";
	u*=2;
	cout << " u: " << u.x << " , " << u.y << endl << endl;


	//
	// Uncomment sections below as you implement more of Vector2d.cpp
	//

	// Test dot product method
	cout << "Test dot product method ";
	s = u.dot(v);
	cout << "u.v = " << s << endl << endl;


	// Test length method
	cout << "Test length method " << endl;
	s = u.length();
	cout << "|u| = " << s << endl << endl;


	// Test normalize method
	cout << "Test normalize method " << endl;
	u.normalize();
	cout << "u: " << u.x << " , " << u.y << endl;
	s = u.length();
	cout << "|u| = " << s << endl << endl;


	// Test += operator
	u.x = 2; u.y = -3;
	cout << "Reset u: " << u.x << " , " << u.y << endl;
	cout << "Test u += v: " << endl;
	u+=v;
	cout << "u: " << u.x << " , " << u.y << endl << endl;


	// Test adding vectors to make new vector
	cout << "Test t = u + v: "; 
    Vector2d t(u+v);
	cout << "t: " << t.x << " , " << t.y << endl << endl;

	// some more tests
	cout << " t = unit vector of u, "; 
	t = u.unit();
	cout << " u: " << u.x << " , " << u.y << endl;
	cout << " t: " << t.x << " , " << t.y << endl;
	s = t.length();
	cout << " |t| = " << s << endl;
    if (u==t)
		cout << "u == t" << endl;
	else
		cout << "u is not equal to t" << endl << endl;


	// Now test comparison of t with unit vector of u
	cout << endl << "Normalizing u." << endl;
	u.normalize();
	cout << "t: " << t.x << " , " << t.y << endl;
	cout << "u: " << u.x << " , " << u.y << endl;
    if (t == u)
		cout << "u == t" << endl;
	else
		cout << "u is not equal to t" << endl;

	cout << endl << "Testing << Operator" << endl;
	cout << t << endl << u;


	// you can add in code here to test the other member functions if you wish

	cin.get();
	return 0;
}

