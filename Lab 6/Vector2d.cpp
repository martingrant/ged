// Vector2d.cpp
// 2D vector class
// Your name here!

#include "Vector2d.h"

//Compare Vector2d
bool Vector2d::operator== ( Vector2d& v) {
	bool result = false;

	if ( (x == v.x) && (y == v.y) ) 
		result = true;
	else
		result = false;

	return result;
}

//Perform negatation
Vector2d Vector2d::operator- () {
	Vector2d result;
	result.x = -x;
	result.y = -y;
	return result;
}

// Assignment operator
const Vector2d& Vector2d::operator= ( const Vector2d& v ) {
	x = v.x;
    y = v.y;
    return *this;
}

//Add Vector2d v to this Vector2d
const Vector2d& Vector2d::operator+= ( const Vector2d& v ) {
	Vector2d result;
	result.x = x += v.x;
	result.y = y += v.y;
	return result;
}

//Subtract Vector2d v from this Vector2d
const Vector2d& Vector2d::operator-= ( const Vector2d& v ) {
	Vector2d result;
	result.x = x -= v.x;
	result.y = y -= v.y;
	return result;
}

// Scale up. Multiply by s 
const Vector2d& Vector2d::operator*= ( const float& s ) { 
	x*=s; 
	y*=s; 
	return *this;
} 

// Scale down - divide by s
const Vector2d& Vector2d::operator/= ( const float& s ) {
	x/=s; 
	y/=s; 
	return *this;
}

// Add two Vector2d's - does not modify value of this Vector2d!
const Vector2d Vector2d::operator+ ( const Vector2d& v ) const {
	Vector2d t;
	t.x = (x + v.x); t.y = (y + v.y);
	return t;
}

// Subtract two Vector2ds - does not modify value of this Vector2d!
const Vector2d Vector2d::operator- ( const Vector2d& v ) const {
	Vector2d t;
	t.x = (x - v.x); t.y = (y - v.y);
	return t;
}

// Scale by a float - does not modify value of this Vector2d!
// calculates: v * s
const Vector2d Vector2d::operator* ( const float& s ) const {
	Vector2d result;
	result.x = x;
	result.y = y;

	return result * s;
}

// Scale down by a float - does not modify value of this Vector2d!
const Vector2d Vector2d::operator/ (float s) const {
Vector2d result;
	result.x = x;
	result.y = y;

	return result / s;
}

//Floating point dot product
const float Vector2d::dot( const Vector2d& v ) const {
	return (x * v.x) + (y * v.y);
}

// Length of this Vector2d
const float Vector2d::length() const {
	return (x*x) + (y*y);
}

// Return a unit Vector2d in same direction as this Vector2d
const Vector2d Vector2d::unit() const {
	float length = (x*x) + (y*y); 
	Vector2d result;
	result.x = (x/length);
	result.y = (y/length);
	return result;
}

//make this a unit Vector2d
void Vector2d::normalize() {
	x = x/Vector2d::length();
	y = y/Vector2d::length();
}
