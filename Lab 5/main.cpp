#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;

template <class T>
T GetMid (T a, T b, T c) {
	if (c < a && b > a || b < a && c > a)
		return a;
	else if (a < b && c > b || c < b && a > b)
		return b;
	else
		return c;
}

/*//////////////// TEMPLATES 

int main() {

	cout << "Middle value is: " << GetMid(1,2,3) << endl;
	cout << "Middle value is: " << GetMid(2,1,3) << endl;
	cout << "Middle value is: " << GetMid(3,2,1) << endl;
	cout << "Middle value is: " << GetMid(3.1,2.1,1.1) << endl;
	string one("one");
	string two("two");
	string three("three");
	cout << "Middle value is: " << GetMid(one, two, three) << endl;
	cout << "Middle value is: " << GetMid(two, three, one) << endl;
	cout << "Middle value is: " << GetMid(three, one, two) << endl;
	cin.get();
}
*/

/*//////////////// Strings, Containers & Iterators 

int main() {

	vector<string> myStrings;
	myStrings.push_back(string("One"));
	myStrings.push_back(string("Two"));
	myStrings.push_back(string("Three"));
	myStrings.push_back(string("Four"));
	myStrings.push_back(string("Five"));
	myStrings.push_back(string("Six"));

	auto stringsItr = myStrings.cbegin();
	while (stringsItr != myStrings.cend() )
		cout << *stringsItr++ << endl;



	return 0;
}
*/

/*//////////////// Strings and Algorithms */

bool lengthCompare(string& a, string& b) {
    return (a.length() < b.length()); 
}

bool alphaSort(string& a, string& b) {
	return (a < b);
}

bool reverseAlphaSort(string& a, string& b) {
	string newA; newA = string ( a.rbegin(), a.rend());
	string newB; newB = string ( b.rbegin(), b.rend());
	return (newA < newB);
}

int main() {

	vector<string> myStrings;
	myStrings.push_back(string("One"));
	myStrings.push_back(string("Two"));
	myStrings.push_back(string("Three"));
	myStrings.push_back(string("Four"));
	myStrings.push_back(string("Five"));
	myStrings.push_back(string("Six"));

	auto stringsItr = myStrings.cbegin();
	while (stringsItr != myStrings.cend() )
		cout << *stringsItr++ << endl;

	cout << endl << "Length sort: " << endl;
	sort(myStrings.begin(), myStrings.end(), lengthCompare);
	stringsItr = myStrings.cbegin();
	while (stringsItr != myStrings.cend() )
		cout << *stringsItr++ << endl;
	cout << endl;

	cout << "Alphabetical sort: " << endl;
	sort(myStrings.begin(), myStrings.end(), alphaSort);
	stringsItr = myStrings.cbegin();
	while (stringsItr != myStrings.cend() )
		cout << *stringsItr++ << endl;
	cout << endl;
	
	cout << "Reverse Alphabetical sort: " << endl; 
	sort(myStrings.begin(), myStrings.end(), reverseAlphaSort);
	stringsItr = myStrings.cbegin();
	while (stringsItr != myStrings.cend() )
		cout << *stringsItr++ << endl;


	return 0;



	/*vector<char> vectorObject(26);
	int i;

	for(i = 0; i <vectorObject.size(); i++) 
		vectorObject[ i ] = 'A'+i;
	
	cout << "Original ordering of vectorObject:";
	for(i = 0; i <vectorObject.size(); i++)
		cout << vectorObject[ i ] << " ";
	cout << endl;

	// sort into desceding order
	sort(vectorObject.begin(), vectorObject.end(), greater<char>());

	cout << "After sorting vectorObject using greater():";
	for(i = 0; i <vectorObject.size(); i++)
		cout << vectorObject[ i ] << " ";
    
    return 0;*/
}