#include "PlayState.h"
#include "game.h"

void PlayState::init(Game &context) {
	xpos = 0.0f;
	ypos = 0.0f;
	xsize = 0.15f;
	ysize = 0.15f;

	targetXPos = 0.0f;
	targetYPos = 0.0f;
	targetXSize = 0.1f;
	targetYSize = 0.1f;

	glClearColor(0.0, 0.0, 0.0, 0.0);

	score = 0;

	std::srand( std::time(NULL) );

	targetXPos = (float)rand()/RAND_MAX - 0.75f;
    targetYPos = (float)rand()/RAND_MAX - 0.75f;
    lastTime = clock();
	label = new Label();
}

void PlayState::draw(SDL_Window * window, Game& context) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window
	// draw player
	glColor3f(1.0,1.0,1.0);
	glBegin(GL_POLYGON);
	  glVertex3f (xpos, ypos, 0.0); // first corner
	  glVertex3f (xpos+xsize, ypos, 0.0); // second corner
	  glVertex3f (xpos+xsize, ypos+ysize, 0.0); // third corner
	  glVertex3f (xpos, ypos+ysize, 0.0); // fourth corner
	glEnd();
	
	//displayString(xpos+(xsize/2.0f), ypos+ysize, "Player");
	label->textToTexture(textFont, "Player");
	label->draw(xpos+(xsize/2.0f), ypos+ysize);

    // draw target
    glColor3f(1.0,0.0,0.0);
    glBegin(GL_POLYGON);
      glVertex3f (targetXPos, targetYPos, 0.0); // first corner
      glVertex3f (targetXPos+targetXSize, targetYPos, 0.0); // second corner
      glVertex3f (targetXPos+targetXSize, targetYPos+targetYSize, 0.0); // third corner
      glVertex3f (targetXPos, targetYPos+targetYSize, 0.0); // fourth corner
    glEnd();
	
	//displayString(targetXPos+(targetXSize/2.0f), targetYPos+targetYSize, "Target");
	label->textToTexture(textFont, "Target");
	label->draw(targetXPos+(targetXSize/2.0f), targetYPos+targetYSize);

    if ( (targetXPos >= xpos) && (targetXPos+targetXSize <= xpos+xsize)	// cursor surrounds target in x
	  && (targetYPos >= ypos) && (targetYPos+targetYSize <= ypos+ysize) ) // cursor surrounds target in y
    {
		score += 100; // congrats, player has scored!
		// randomize the new target position
		targetXPos = (float)rand()/RAND_MAX - 0.75f;
		targetYPos = (float)rand()/RAND_MAX - 0.75f;
    }

    glColor3f(1.0,1.0,1.0);
    currentTime = clock();
    // On some systems, CLOCKS_PER_SECOND is 1000, which makes the arithmetic below redundant
    // - but this is not necessarily the case on all systems
    float milliSecondsPerFrame = ((currentTime - lastTime)/(float)CLOCKS_PER_SEC*1000);

    // Print out the score and frame time information
    std::stringstream strStream;
    strStream << "Score:" << score;
    strStream << "          ms/frame: " << milliSecondsPerFrame;
   
	//displayString(-0.9,0.9, strStream.str().c_str());

	label->textToTexture(textFont, strStream.str().c_str());
	label->draw(-0.9,0.9);

    lastTime = clock();
	SDL_GL_SwapWindow(window); // swap buffers
}

bool PlayState::handleSDLEvent(SDL_Event const &sdlEvent, Game &context) {
	if (sdlEvent.type == SDL_KEYDOWN) {
		//std::cout << "Scancode: " << sdlEvent.key.keysym.scancode  ;
        //std::cout <<  ", Name: " << SDL_GetKeyName( sdlEvent.key.keysym.sym ) << std::endl;
		switch( sdlEvent.key.keysym.sym ) {
			case SDLK_UP:
			case 'w': case 'W': 
				ypos += 0.05f;
				break;
			case SDLK_DOWN:
			case 's': case 'S':
				ypos -= 0.05f;
				break;
			case SDLK_LEFT:
			case 'a': case 'A': 
				xpos -= 0.05f;
				break;
			case SDLK_RIGHT:
			case 'd': case 'D':
				xpos += 0.05f;
				break;
			case SDLK_ESCAPE:
					context.setState(context.getMainMenuState());
			default:
				break;
		}
		return true;
	}
}