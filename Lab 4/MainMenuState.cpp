#include "MainMenuState.h"
#include "game.h"

void MainMenuState::init(Game& context) {
	label = new Label();
}

bool MainMenuState::handleSDLEvent(SDL_Event const &sdlEvent, Game &context) {
	bool running = true;
	if (sdlEvent.type == SDL_KEYDOWN)
	{
		//std::cout << "Scancode: " << sdlEvent.key.keysym.scancode  ;
        //std::cout <<  ", Name: " << SDL_GetKeyName( sdlEvent.key.keysym.sym ) << std::endl;
		if (sdlEvent.key.keysym.sym == SDLK_RETURN)
		{
			context.setState(context.getPlayState());
			running = true;
		}
		if (sdlEvent.key.keysym.sym == SDLK_ESCAPE)
		{
			running = false;
			SDL_DestroyWindow(context.window);
			SDL_Quit();
		}
	}
	return running;
}

void MainMenuState::draw(SDL_Window* window, Game& context) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window
	label->textToTexture(textFont, "Main Menu");
	label->draw(-0.1,0.5);
	label->textToTexture(textFont, "<Enter> Back To Game");
	label->draw(-0.25,0.2);
	label->textToTexture(textFont, "<Escape> Quit");
	label->draw(-0.15,0);
	SDL_GL_SwapWindow(window); 
}