#include <string>
#include <iostream>
// Instead of importing the entire std namespace, we can be
// specific about which std library classes and objects are to be included:
using std::string;
using std::cout;
using std::endl;
using std::cin;


// Abstract base class: no implementation
class AbstractNPC {
public:
	virtual void render() = 0;
	virtual int getMana() = 0;
	virtual int getHealth() = 0;
	virtual ~AbstractNPC(){}
};

// NPC implements AbstractNPC - all methods defined inline
class NPC: public AbstractNPC {
private:
	string name;
protected: 
	int health;
	int mana;
public:
	NPC(string basename, int m, int h) { name.assign(basename); mana = m; health = h;} // assign is a string function
	NPC(char * basename, int m, int h) { name.assign(basename); mana = m; health = h;} // to set string contents
	void render() { cout << name ;}
	int getMana() {return mana;}
	int getHealth() {return health;}
	~NPC() { cout << "Deleting NPC object " << name << endl; }
};

// NPCDecorator implements AbstractNPC
class NPCDecorator: public AbstractNPC {
private:
	AbstractNPC * npc;
public:
	NPCDecorator(AbstractNPC *n) { npc = n; }
	void render() { npc->render(); } // delegate render to npc data member
	int getMana() { return npc->getMana(); }
	int getHealth() { return npc->getHealth(); }
	~NPCDecorator() {delete npc;}
};

// Elite, Captain and Shaman are all types of NPCDecorator
class Elite: public NPCDecorator {
public:
	Elite(AbstractNPC *n): NPCDecorator(n) { }
	int getMana() { return NPCDecorator::getMana() + 20;}
		int getHealth() { return NPCDecorator::getHealth() + 20;}
		void render() {
		cout << "Elite " ; // render special features
		NPCDecorator::render(); // delegate to base class
	
	}
		~Elite() { cout << "Deleting Elite decorator" << endl; }
};

class Captain: public NPCDecorator {
public:
	Captain(AbstractNPC *n): NPCDecorator(n) { }
	int getMana() { return NPCDecorator::getMana(); }
	int getHealth() { return NPCDecorator::getHealth() + 40;}
	void render() {
		cout << "Captain "; // render special features
		NPCDecorator::render(); // delegate to base class
	
	}
	~Captain() { cout << "Deleting Captain decorator" << endl; }
};
   
class Shaman: public NPCDecorator {
public:
	Shaman(AbstractNPC *n): NPCDecorator(n) { }
	int getMana() { return NPCDecorator::getMana() + 60;}
	int getHealth() { return NPCDecorator::getHealth();}
	void render() {
		NPCDecorator::render(); // delegate to base class
		cout << " Shaman "; // render special features *after* base
		
		
	}
	~Shaman() { cout << "Deleting Shaman decorator" << endl; }
};

// Program entry point
int main(int argc, char **argv)
{
AbstractNPC *monsterArray[10];

monsterArray[0]= new Elite(new NPC("orc", 10, 40));
monsterArray[1]= new Captain(new NPC("orc", 10, 40));
monsterArray[2]= new Shaman(new NPC("orc", 10, 40));
monsterArray[3]= new Elite(new Captain(new NPC("orc", 10, 40)));
monsterArray[4]= new Elite(new NPC("Troll", 10, 100));
monsterArray[5]= new Captain(new NPC("Troll", 10, 100));
monsterArray[6]= new Shaman(new NPC("Troll", 10, 100));
monsterArray[7]= new Elite(new Captain(new NPC("Troll", 10, 100)));
monsterArray[8]= new Elite(new NPC("Goblin", 25, 25));
monsterArray[9]= new Captain(new NPC("Goblin", 25, 25));

for(int i=0; i<10; i++){
	monsterArray[i]->render();
	cout << endl << " " << "Mana: " << monsterArray[i]->getMana() << " " << "Health: " << monsterArray[i]->getHealth() << endl;
	

} 
for (int i=0; i<10; i++){
	cout << ".  .  ." << endl;
	delete monsterArray[i];
	cout << endl << endl;
}
	cin.get();
	return 0;
}

