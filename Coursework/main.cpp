//////////////////////////////////////
// UWS Game Engine Design Coursework 2012
//
// Galactic Marine
//
// B00221736
//
//////////////////////////////////////

#include "Game.h"

#if _DEBUG
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif

int main(int argc, char *argv[])
{
	// Create game and run it
    Game *newGame = new Game();

	newGame->run();
	
	delete newGame;

    return 0;
}
