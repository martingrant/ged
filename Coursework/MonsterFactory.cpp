#include "MonsterFactory.h"
#include <string>

// Creates newMonster object and returns it when this function is called
// also calls createMonster function, passing in the parameter "type"
Monster* MonsterFactory::orderMonster(std::string type) {
	Monster* newMonster;
	newMonster = createMonster(type);

	return newMonster;
}

// "type" is passed in as a string, depending on what type is, it will let
// sub-classes of Monster decide which monster to create and assign to the newMonster object
Monster* MonsterFactory::createMonster(std::string type) {
	if ( type == "fodder" ) return new MonsterFodder();
    if ( type == "brute" ) return new MonsterBrute();
	if ( type == "raider" ) return new MonsterRaider();
    return NULL;
}