#ifndef MONSTERFODDER_H
#define MONSTERFODDER_H

#include "Monster.h"

class MonsterFodder: public Monster {
public:
	~MonsterFodder() { std::cout << "Deleted MonsterFodder" << std::endl; delete label; } // need a virtual destructor
	MonsterFodder();
	void draw();
	float getXPos(){return xpos;}
	float getYPos(){return ypos;}
	float getXSize(){return xsize;}
	float getYSize(){return ysize;}
	void update();
	bool getHasCollided() {return hasCollided;}
	void sethasCollided(bool value) {hasCollided = value;}
	void setCombatPos();
	int getHealth() {return health;}
	int getStrength() {return strength;}
	int getSpeed() {return speed;}
	int getDollars() {return dollars;}
	void setHealth(int newHealth) {health = health - newHealth;}

	// calculate value between 1 and 100, if <= 5 then item and dropped (this is referenced by other code to drop the item)
	bool getDropRate() {
		dropRate = rand() % 100+1; 
		std::cout << "monster drop rate: " << dropRate << std::endl;
		if(dropRate <= 5)
			return true;
		else
			return false;
	}    

	void setDead(bool value) { 
		dead = value;
	}

	bool getDead() { 
		return dead;
	}
};

#endif
