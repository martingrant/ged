#include "StateCombat.h"
#include "Game.h"

void StateCombat::init(Game &context) {
	label = new Label();
	fighting = false;
}

void StateCombat::draw(SDL_Window * window, Game& context) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window
	
	// assign player to context player parameter to reference easily
	player = context.player;
	player->draw();

	// draw player stats on-screen
	std::stringstream playerStrStream1;
	playerStrStream1 << "Health:" << player->getHealth();
	label->textToTexture(playerStrStream1.str().c_str());
    label->draw(-0.2f, -0.1f, 0.0015f);

	std::stringstream playerStrStream2;
	playerStrStream2 << "Strength:" << player->getStrength();
	label->textToTexture(playerStrStream2.str().c_str());
    label->draw(-0.2f, -0.15f, 0.0015f);

	std::stringstream playerStrStream3;
	playerStrStream3 << "Speed:" << player->getSpeed();
	label->textToTexture(playerStrStream3.str().c_str());
    label->draw(-0.2f, -0.2f, 0.0015f);

	std::stringstream playerStrStream4;
    playerStrStream4 << "Dollars:" << player->getDollars();
	label->textToTexture(playerStrStream4.str().c_str());
    label->draw(-0.2f, -0.25f, 0.0015f);

	// assign the currently collided monster to currentMonster for easier reference
	for(int i=0; i<9; i++) {
		if(context.monArr[i] != NULL) {
			if(context.monArr[i]->getHasCollided() == true) {
				currentMonster = context.monArr[i];
			}
		}
	}

	//draw current monster and its stats on-screen
	currentMonster->setCombatPos();
	currentMonster->draw();
			
	std::stringstream monsterStrStream1;
	monsterStrStream1 << "Health:" << currentMonster->getHealth();
	label->textToTexture(monsterStrStream1.str().c_str());
	label->draw(0.2f, -0.1f, 0.0015f);

	std::stringstream monsterStrStream2;
	monsterStrStream2 << "Strength:" << currentMonster->getStrength();
	label->textToTexture(monsterStrStream2.str().c_str());
	label->draw(0.2f, -0.15f, 0.0015f);

	std::stringstream monsterStrStream3;
	monsterStrStream3 << "Speed:" << currentMonster->getSpeed();
	label->textToTexture(monsterStrStream3.str().c_str());
	label->draw(0.2f, -0.20f, 0.0015f);

	std::stringstream monsterStrStream4;
	monsterStrStream4 << "Dollars:" << currentMonster->getDollars();
	label->textToTexture(monsterStrStream4.str().c_str());
	label->draw(0.2f, -0.25f, 0.0015f);

	SDL_GL_SwapWindow(window); 
}

void StateCombat::handleSDLEvent(SDL_Event const &sdlEvent, Game &context) {
	if (sdlEvent.type == SDL_KEYDOWN) {
		// pause fighting for keypresses of ENTER
		if (sdlEvent.key.keysym.sym == SDLK_RETURN) {
			if( fighting == false )
				combat();
			else if ( fighting == true ) {
				// if monster has been killed, take player to map screen
				if( currentMonster->getHealth() <= 0) {
					postCombatAlive();
					fighting = false;
					context.setState(context.getStateMap());
				} else if (player->getHealth() <= 0) {
					// player has been killed, game over screen
					postCombatDead(context);
					fighting = false;
				}
			}
		}
	}
}

// decide who gets to attack first
// if speed of player and monster are equal, choose at random
// highest speed gets to hit first
void StateCombat::preCombat() {
	if(player->getSpeed() == currentMonster->getSpeed())
		attackTurn = rand() % 2;     
	if(player->getSpeed() > currentMonster->getSpeed())
		attackTurn = 1;     
	else
		attackTurn = 0;
}

void StateCombat::inCombat() {
	// amount of damage done to oponent is random between 2 and strength 
	int playerHit = rand() % (player->getStrength()-1) + 2;
	int monsterHit = rand() % (currentMonster->getStrength()-1) + 2;

	std::cout << "player hit monster " << playerHit << "    " << "   monster hit player" << monsterHit << std::endl;

	// then take turns attacking opponent...
	if( attackTurn == 1 ) {
		currentMonster->setHealth(playerHit);
		//std::cout << "player hit monster" << std::endl;
		attackTurn = 0;
	}
	if( attackTurn == 0 ) {
		player->damageTaken(monsterHit);
		//std::cout << "monster hit player" << std::endl;
		attackTurn = 1;
	}
}

void StateCombat::postCombatAlive() {
	// if the player has won then reset player to original position and heal them for half of damage taken during fight
	player->resetPos();
	int heal = (player->getMaxHealth() - player->getHealth())/2;
	player->setHealth(heal + player->getHealth());
	// give the player dollars that the monster had
	player->setDollars(currentMonster->getDollars());

	// set the monster to dead
	currentMonster->setDead(true);

	// if the monster has dropped an item....
	if(currentMonster->getDropRate() == true) {
		int itemDrop = rand() % 100 + 1;

		// decide which item it should be, create random number and if
		// >= 20 then create a stimulant, > 20 or <= 40 create combat pack or
		// then create a health pack
		if( itemDrop >= 20 ) {
			monsterDrop = new ItemStimulant(new Item("Stimulant", 0, 0, 1));
			std::cout << "Monster dropped" << "(rate: " << itemDrop << ") " << "Item Stimulant" << std::endl; }
		else if( itemDrop > 20 && itemDrop <= 40) {
			monsterDrop = new ItemCombatPack(new Item("Combat Pack", 2, 0, 0));
			std::cout << "Monster dropped" << "(rate: " << itemDrop << ") " << "Item Combat Pack" << std::endl; }
		else {
			monsterDrop = new ItemHealthPack(new Item("Health Pack", 0, 1, 0));
			std::cout << "Monster dropped" << "(rate: " << itemDrop << ") " << "Item Health Pack" << std::endl; }

		// apply the effect to the player immediately, no need to display the object on-screen
		monsterDrop->itemEffect(player);
	}
}

void StateCombat::postCombatDead(Game& context) {
	// switch to game over/won screen
	context.setState(context.getStateGameOver());
}

void StateCombat::combat() {
	// setup combat loop, choosing who will hit first
	// continue until player or monster has died
	do {
	preCombat();
	inCombat();
	} while (player->getHealth() > 0 && currentMonster->getHealth() > 0);

	fighting = true;
}

void StateCombat::update(Game& context) { }
