#include "Game.h"

int Game::instances = 0;

Game::Game() {
	window = setupRC(glContext);

	// create state objects and initialise below
	stateMap = new StateMap();
	stateMainMenu = new StateMainMenu();
	stateIntro = new StateIntro();
	stateCredits = new StateCredits();
	stateCombat = new StateCombat();
	stateCharSelect = new StateCharSelect();
	stateGameOver = new StateGameOver();

	stateMap->init(*this);
	stateMainMenu->init(*this);
	stateIntro->init(*this);
	stateCredits->init(*this);
	stateCombat->init(*this);
	stateCharSelect->init(*this);
	stateGameOver->init(*this);

	// current/starting state is intro, can be changed for faster debugging (e.g. working on specific state)
	stateCurrent = stateIntro;

	// dont allow multiple instances of game
	instances++;
	if (instances > 1)
		Label::exitFatalError("Attempt to create multiple game instances");


	// create player, monster factory

	player = new Player();
	monsterFactory = new MonsterFactory();

	// create and init monster and item objects
	monArr[0] = monsterFactory->orderMonster("fodder");
	monArr[1] = monsterFactory->orderMonster("fodder");
	monArr[2] = monsterFactory->orderMonster("fodder");
	monArr[3] = monsterFactory->orderMonster("fodder");
	monArr[4] = monsterFactory->orderMonster("fodder");
	monArr[5] = monsterFactory->orderMonster("brute");
	monArr[6] = monsterFactory->orderMonster("brute");
	monArr[7] = monsterFactory->orderMonster("brute");
	monArr[8] = monsterFactory->orderMonster("raider");

	itemArr[0]= new ItemStimulant(new Item("Stimulant", 0, 0, 1));
	itemArr[1]= new ItemHealthPack(new Item("Health Pack", 0, 1, 0));
	itemArr[2]= new ItemHealthPack(new Item("Health Pack", 0, 1, 0));
	itemArr[3]= new ItemCombatPack(new Item("Combat Pack", 2, 0, 0));
	itemArr[4]= new ItemCombatPack(new Item("Combat Pack", 2, 0, 0));
}

Game::~Game() {
	delete stateCurrent;
	delete stateMap;
	delete stateMainMenu;
	delete stateIntro;
	delete stateCredits;
	delete stateCombat;
	delete stateCharSelect;
	delete stateGameOver;

	delete player;
	delete monsterFactory;
	delete itemArr;
	delete monArr;

    SDL_DestroyWindow(window);
	SDL_Quit();
}

void Game::run() {
	bool running = true; // set running to true
	SDL_Event sdlEvent; // variable to detect SDL events

	std::cout << "Progress: About to enter main loop" << std::endl;

	while (running)	
	{
		while (SDL_PollEvent(&sdlEvent))
		{
			if (sdlEvent.type == SDL_QUIT)
				running = false;
			else
				// detect inputs for current state
				stateCurrent->handleSDLEvent(sdlEvent, *this);
		}
		// draw and update currently selected state
		stateCurrent->draw(window, *this);
		stateCurrent->update(*this);
	}
}

SDL_Window * Game::setupRC(SDL_GLContext &context) // setup SDL window
{
	TTF_Font* textFont;
	SDL_Window *window;
    if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
        Label::exitFatalError("Unable to initialize SDL"); 

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);

    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1); 
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8); 

	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);
 
    window = SDL_CreateWindow("Galactic Marine - GED 2012 Project B00221736",
		SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        800, 600, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
	if (!window) 
       Label::exitFatalError("Unable to create window");
 
    context = SDL_GL_CreateContext(window); 
    SDL_GL_SetSwapInterval(1); 

	if (TTF_Init()== -1)
		Label::exitFatalError("TTF failed to initialise.");
	
	textFont = TTF_OpenFont("MavenPro-Regular.ttf", 24);
	if (textFont == NULL)
		Label::exitFatalError("Failed to open font.");

	return window;
}
